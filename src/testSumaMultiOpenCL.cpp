#define helper_matA(r, c, w) ((*matA)[(r)*(w) + (c)])
#define helper_matB(r, c, w) ((*matB)[(r)*(w) + (c)])
#define helper_matC(r, c, w) ((*matC)[(r)*(w) + (c)])
#define helper_resultMatrix(r, c, w) ((*resultMatrix)[(r)*(w) + (c)])
#define MAX_SOURCE_SIZE (0x100000)
#include <iostream>
#include <vector>
#include <time.h>
#include <math.h>
#include <random>
#include <vector>
#include<chrono>
#include<iomanip>
#include <fstream>
#include <CL/cl2.hpp>
using namespace std;
//funciones de la suma
float *doOperationSum(vector<float> *matA,
		vector<float> *matB,unsigned int n);
//float *doOperationMul(unsigned int n);
//funciones de la multipliacion
vector<float> *createSquareMatrix(unsigned int matrixSize);
float *squareMatrixMultiply(
	vector<float> *matA,
	vector<float> *matB);

int main() {
	ofstream fs("AddMatrix.csv");
	ofstream fm("MultMatrix.csv");
	float *ptic,*ptoc;
	//declaracion de las matrices

	//Elements in each array
	vector<int> testSizes = {
				16,32,64,128,256,512,1024,2048,4096
		};
		fs << "n, HostTime, DeviceTime, sk" << endl;
		fm << "n, HostTime, DeviceTime, sk" << endl;
		for (unsigned int n : testSizes)
				for(unsigned int j=0;j<200;j++){
					vector<float> *matA = createSquareMatrix(n);
					vector<float> *matB = createSquareMatrix(n);
					vector<float> *matC = nullptr;
					ptic=doOperationSum(matA,matB,n);
					//cout<<"sum "<<j<<endl;
					//cout<<"["<<n<<"]"<<"tiempo de regreso sum: "<<ptic[0]<<","<<ptic[1]<<","<<ptic[2]<<endl;
					fs<<n<<","<<ptic[0]<<","<<ptic[1]<<","<<ptic[2]<<endl;
					ptoc = squareMatrixMultiply(matA,matB);
					//cout<<"["<<n<<"]"<<"tiempo de regreso multi: "<<ptoc[0]<<","<<ptoc[1]<<","<<ptoc[2]<<endl;
					fm<<n<<","<<ptoc[0]<<","<<ptoc[1]<<","<<ptoc[2]<<endl;

					cout<<"["<<j<<","<<n<<"]"<<endl;
						delete matA;
						delete matB;
						delete matC;
			}
		fs.close();
		fm.close();

		delete ptic;
		delete ptoc;

	return 0;
}

vector<float> *createSquareMatrix(unsigned int matrixSize){

	  default_random_engine generator(
			  chrono::steady_clock::now().time_since_epoch().count() );
	  uniform_real_distribution<float> distribution(0.0,1.0);
	  vector<float> *resultMatrix =  new vector<float>(matrixSize*matrixSize);
	  for(unsigned int k = 0; k<matrixSize*matrixSize; k++) {
		  (*resultMatrix)[k] = distribution(generator);
	  }
	  return resultMatrix;
}

float *doOperationSum(vector<float> *matA,
		vector<float> *matB,unsigned int n)
{
 const char *programSourceSum=
			"__kernel							\n"
			"void matrixadd(__global float *A,		\n"
			"	__global float *B,				\n"
			"	__global float *C)				\n"
			"{										\n"
			"	int idx = get_global_id(0);			\n"
			"	C[idx] = A[idx] + B[idx];			\n"
			"}";
 	 //declaracion de variables

	//para asignar los valores random


	int elements = n*n;

	 //int elements = testSizes[6];
	 //generator for uniform_real_distribution
	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0,1.0);

	//obtener el tama�o de la matriz y asignandolo a datasize
	size_t datasize = sizeof(int)*elements;
	//allocate the memory size for each matrix
	//matC = (float*)malloc(datasize);
	vector<float> *matC = new vector<float>(elements,0.0f);

	auto t1_1 = chrono::steady_clock::now();
	//Do the add operation without OpenCL
	for(unsigned int s=0;s<n*n;s++){
			(*matC)[s] =  (*matA)[s] + (*matB)[s];
	}
	auto t1_2 = chrono::steady_clock::now();

	auto int_ms = chrono::duration_cast<chrono::milliseconds>(t1_2 - t1_1);

	chrono::duration<float, milli> calcTime1 = t1_2 - t1_1;
	//cout <<"["<<n<<"]"<< " Operation took without OpenCL " << fixed << setprecision(9) << calcTime1.count() << " ms" <<endl;

	//check the status of errors
	cl_int status;
	//STEP 1: Discover and initialize the platforms
	cl_uint numPlatforms = 0;
	cl_platform_id *platforms = NULL;
	//Use clGetPlatformIDs() to retrieve the number of platforms
	status = clGetPlatformIDs(0, NULL, &numPlatforms);
	//Allocate enough space for each platform
	platforms = (cl_platform_id*)malloc(numPlatforms * sizeof(cl_platform_id));
	//Fill in platforms with clGetPlatformIDs()
	status = clGetPlatformIDs(numPlatforms, platforms, NULL);

	//STEP 2: Discover and initialize the devices
	cl_uint numDevices = 0;
	cl_device_id *devices = NULL;
	//Use clGetDeviceIDs() to retrieve the number of devices present
	status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
	//Allocate enough space for each device
	devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
	//Fill in device with clGetDeviceIDs()
	status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);

	//STEP 3: Create a context
	cl_context context = NULL;
	//Create a context using clCreateContext() and associate it with the devices
	context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);

	//STEP 4: Create a command queue
	cl_command_queue cmdQueue;
	//Create a command queue using clCreateCommandQueue(),
	//and associate it with the device you want to execute on
	cmdQueue = clCreateCommandQueueWithProperties(context, devices[0], 0, &status);

	//STEP 5: Create device buffers
	cl_mem bufferA; //Input array on the device
	cl_mem bufferB; //Input array on the device
	cl_mem bufferC; //output array on the device
	auto t1 = chrono::steady_clock::now();
	//Use clCreateBuffer() to create a buffer object (d_A)
	//that will contain the data from the host array A
	bufferA = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL, &status);
	//Use clCreateBuffer() to create a buffer object (d_B)
	//that will contain the data from the host array B
	bufferB = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL, &status);
	//Use clCreateBuffer() to create a buffer object (d_C)
	//with enough space to hold the output data
	bufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, datasize, NULL, &status);

	//STEP 6: Write host data to device buffers
	//Use clEnqueueWriteBuffer() to write input array A to
	//the device buffer bufferA
	status = clEnqueueWriteBuffer(cmdQueue, bufferA, CL_FALSE, 0, datasize,&(*matA)[0], 0, NULL, NULL);
	//Use clEnqueueWriteBuffer() to write input array B to
	//the device buffer bufferB
	status = clEnqueueWriteBuffer(cmdQueue, bufferB, CL_FALSE, 0, datasize, &(*matB)[0], 0, NULL, NULL);

	//STEP 7: Create and compile the program
	//Create a program using clCreateProgramWithSource()
	cl_program program = clCreateProgramWithSource(context, 1, (const char**)&programSourceSum, NULL, &status);
	//Build(compile) the program for the devices with
	//clBuildProgram()
	status = clBuildProgram(program, numDevices, devices, NULL, NULL, NULL);


	//STEP 8: Create the kernel
	cl_kernel kernel = NULL;
	//Use clCreateKernel() to create a kernel from the
	//vector addition function (named "matrixadd")
	kernel = clCreateKernel(program, "matrixadd", &status);

	//STEP 9: Set the kernel arguments
	//Associate the input and output buffers with the
	//kernel
	//using clSetKernelArg()
	status  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufferA);
	status |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufferB);
	status |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufferC);

	//STEP 10: Configure the work-item structure
	//Define an index space (global work size) of work
	//items for
	//execution. A workgroup size (local work size) is not
	//required.
	//but can be used.
	size_t globalWorkSize[1];
	//There are'elements' work-items
	globalWorkSize[0] = elements;

	//STEP 11: Enqueue the kernel for execution
	//Execute the kernel by using
	//clEnqueueNDRangeKernel().
	//'globalWorkSize' is the ID dimension of the
	//work-items
	status = clEnqueueNDRangeKernel(cmdQueue, kernel, 1, NULL, globalWorkSize, NULL, 0, NULL, NULL);

	//STEP 12: Read the output buffer back to the host
	//Use clENqueueReadBuffer() to read the OpenCL output
	//buffer (bufferC)
	//to the host output array(C)
	clEnqueueReadBuffer(cmdQueue, bufferC, CL_TRUE, 0, datasize,(void *)&(*matC)[0], 0, NULL, NULL);

	//show the data of matrix's
	/*for  (int i = 0; i < elements; i++){
	cout<<"["<< i<<"]: " << float(A[i]) <<"/ " << float (B[i]) <<" = " <<float (C[i])<< endl;
	}*/

	auto t2 = chrono::steady_clock::now();

	auto int_ms2 = chrono::duration_cast<chrono::milliseconds>(t2 - t1);

	chrono::duration<float, milli> calcTime2 = t2 - t1;
	//cout << "["<<n<<"]"<<" Operation took with OpenCL " << fixed << setprecision(9) << calcTime2.count() << " ms" <<endl;
	//cin.get();
	 float c1= calcTime1.count();
	 float c2= calcTime2.count();
	 float c3=c1/c2;
	 float tic[2];
	  tic[0] = c1;
	  tic[1] = c2;
	  tic[2] = c3;
	float *ptic=tic;

	//cout <<"tiempo dentro de la fun sum: "<<tic[0]<<" "<<tic[1]<<" "<<tic[2]<<endl;

	//cout<<"dentro de matrixSum "<<tic[0]<<" "<<tic[1]<<" "<<c1<<" "<<c2<<endl;
	//STEP 13: Release OpenCL resources
	//Free OpenCL resources
	clReleaseKernel(kernel);
			clReleaseProgram(program);
			clReleaseCommandQueue(cmdQueue);
			clReleaseMemObject(bufferA);
			clReleaseMemObject(bufferB);
			clReleaseMemObject(bufferC);
			clReleaseContext(context);

			//Free host resources
			/*free(matA);
			free(matB);
			free(matC);*/
			free(platforms);
			free(devices);

			return ptic;
	}


float *squareMatrixMultiply(
		vector<float> *matA,
		vector<float> *matB) {

	const char *programSourceMul=
	"__kernel void      				\n"
	"MatrixMult(__global float* A, 	\n"
	 "         __global float* B, 	\n"
	  "        __global float* C, 	\n"
	   "       int rows, int cols)	\n"
	"{	  							\n"
	 "  int r = get_global_id(0); 	\n"
	  " int c = get_global_id(1);	\n"
	   " float value = 0;			\n"
	   "for (int k = 0; k < rows; ++k)	\n"
	   "{							\n"
	    "  float elementA = A[c * rows + k];	\n"
	     " float elementB = B[k * cols + r];	\n"
	      "value += elementA * elementB;		\n"
	   "}   								\n"
	   "C[c * rows + r] = value;			\n"
	"}";

	unsigned int matrixSize = sqrt(matA->size());
	float* h_C = (float*) malloc(matrixSize*matrixSize);
	vector<float> *resultMatrix = new vector<float>(matrixSize*matrixSize,0.0f);
	auto t1 = chrono::steady_clock::now();
	for(unsigned int r=0;r<matrixSize;r++) {
		for(unsigned int c=0;c<matrixSize;c++) {
			float accum = 0.0f;
			for(unsigned int k=0;k<matrixSize;k++) {
				accum += helper_matA(r,k,matrixSize)*helper_matB(k,c,matrixSize);
			}
			helper_resultMatrix(r,c,matrixSize) = accum;
		}
	}
	auto t2 = chrono::steady_clock::now();

	auto int_ms2 = chrono::duration_cast<chrono::milliseconds>(t2 - t1);

	chrono::duration<float, milli> calcTime2_1 = t2 - t1;
	//cout<<"your time multiply is: "<<calcTime2_1.count()<<endl;
	/*const unsigned int cols= matrixSize;
	const unsigned int rows= matrixSize;
	cl_int status;
		//STEP 1: Discover and initialize the platforms
		cl_uint numPlatforms = 0;
		cl_platform_id *platforms = NULL;
		//Use clGetPlatformIDs() to retrieve the number of platforms
		status = clGetPlatformIDs(0, NULL, &numPlatforms);
		//Allocate enough space for each platform
		platforms = (cl_platform_id*)malloc(numPlatforms * sizeof(cl_platform_id));
		//Fill in platforms with clGetPlatformIDs()
		status = clGetPlatformIDs(numPlatforms, platforms, NULL);

		//STEP 2: Discover and initialize the devices
		cl_uint numDevices = 0;
		cl_device_id *devices = NULL;
		//Use clGetDeviceIDs() to retrieve the number of devices present
		status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
		//Allocate enough space for each device
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		//Fill in device with clGetDeviceIDs()
		status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);

		//STEP 3: Create a context
		cl_context context = NULL;
		//Create a context using clCreateContext() and associate it with the devices
		context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);


		//STEP 4: Create a command queue
		cl_command_queue cmdQueue;
		//Create a command queue using clCreateCommandQueue(),
		//and associate it with the device you want to execute on
		cmdQueue = clCreateCommandQueueWithProperties(context, devices[0], 0, &status);

		//STEP 5: Create device buffers
		cl_mem bufferA; //Input array on the device
		cl_mem bufferB; //Input array on the device
		cl_mem bufferC; //output array on the device
		cl_mem bufferRows; //Input rows on the device
		cl_mem bufferCols; //Input cols on the device
	*/	auto t3 = chrono::steady_clock::now();
		//Use clCreateBuffer() to create a buffer object (d_A)
		//that will contain the data from the host array A
	/*	bufferA = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, matrixSize*matrixSize, &(*matA)[0], &status);

		//Use clCreateBuffer() to create a buffer object (d_B)
		//that will contain the data from the host array B
		bufferB = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, matrixSize*matrixSize, &(*matB)[0], &status);
		//Use clCreateBuffer() to create a buffer object (d_C)
		//with enough space to hold the output data
		bufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, matrixSize*matrixSize, NULL, &status);

		bufferRows = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, matrixSize*matrixSize, (void *)&rows, &status);
		bufferCols = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, matrixSize*matrixSize, (void *)&cols, &status);

		//STEP 6: Write host data to device buffers
		//Use clEnqueueWriteBuffer() to write input array A to
		//the device buffer bufferA
		status = clEnqueueWriteBuffer(cmdQueue, bufferA, CL_FALSE, 0, matrixSize*matrixSize,&(*matA)[0], 0, NULL, NULL);
		//Use clEnqueueWriteBuffer() to write input array B to
		//the device buffer bufferB
		status = clEnqueueWriteBuffer(cmdQueue, bufferB, CL_FALSE, 0, matrixSize*matrixSize,&(*matB)[0], 0, NULL, NULL);
		status = clEnqueueWriteBuffer(cmdQueue, bufferRows, CL_FALSE, 0, matrixSize,(&rows), 0, NULL, NULL);
		status = clEnqueueWriteBuffer(cmdQueue, bufferCols, CL_FALSE, 0, matrixSize,(&cols), 0, NULL, NULL);

		//STEP 7: Create and compile the program
		//Create a program using clCreateProgramWithSource()
		cl_program program = clCreateProgramWithSource(context, 1,(const char**)&programSourceMul,
				NULL, &status);

		//Build(compile) the program for the devices with
		//clBuildProgram()
		status = clBuildProgram(program, numDevices, devices, NULL, NULL, NULL);


		//STEP 8: Create the kernel
		cl_kernel kernel = NULL;
		//Use clCreateKernel() to create a kernel from the
		//vector addition function (named "MatrixMult")
		kernel = clCreateKernel(program, "MatrixMult", &status);

		//STEP 9: Set the kernel arguments
		//Associate the input and output buffers with the
		//kernel
		//using clSetKernelArg()
		status  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufferA);
		status |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufferB);
		status |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufferC);
		status |= clSetKernelArg(kernel,3,sizeof(cl_uint),(void*)&rows);
		status |= clSetKernelArg(kernel,4,sizeof(cl_uint),(void*)&cols);

		//STEP 10: Configure the work-item structure
		//Define an index space (global work size) of work
		//items for
		//execution. A workgroup size (local work size) is not
		//required.
		//but can be used.
		//size_t globalWorkSize[1];

		size_t globalWorkSize[2]= { rows, cols };
		//globalWorkSize[0] = rows;
		//globalWorkSize[1] = cols;
		//cout<<"rows and cols: "<<globalWorkSize[0]<<","<<globalWorkSize[1]<<endl;
		//There are'elements' work-items
		//STEP 11: Enqueue the kernel for execution
		//Execute the kernel by using
		//clEnqueueNDRangeKernel().
		//'globalWorkSize' is the ID dimension of the
		//work-items
		status = clEnqueueNDRangeKernel(cmdQueue, kernel, 1, nullptr, globalWorkSize, nullptr, 0, nullptr, nullptr);

		//STEP 12: Read the output buffer back to the host
		//Use clENqueueReadBuffer() to read the OpenCL output
		//buffer (bufferC)
		//to the host output array(C)
		clEnqueueReadBuffer(cmdQueue, bufferC, CL_TRUE, 0, sizeof(cl_float)*rows*cols,h_C, 0, NULL, NULL);

		//show the data of matrix's
		/*for  (int i = 0; i < elements; i++){
		cout<<"["<< i<<"]: " << float(A[i]) <<"/ " << float (B[i]) <<" = " <<float (C[i])<< endl;
		}*/

		auto t4 = chrono::steady_clock::now();

		auto int_ms3 = chrono::duration_cast<chrono::milliseconds>(t4 - t3);

		chrono::duration<float, milli> calcTime2_2 = t4 - t3;
		//cout << "["<<n<<"]"<<" Operation took with OpenCL " << fixed << setprecision(9) << calcTime2.count() << " ms" <<endl;
		//cin.get();
		 float c1= calcTime2_1.count();
		 float c2= calcTime2_2.count();
		 float c3=c1/c2;
		 float toc[2];
		  toc[0] = c1;
		  toc[1] = c2;
		  toc[2] = c3;
		float *ptoc=toc;
		//cout<<"multi in func: "<<c1<<","<<c2<<","<<c3<<","<<ptoc[0]<<","<<ptoc[1]<<","<<ptoc[2]<<endl;

		//cout <<"tiempo dentro de la fun sum: "<<tic[0]<<" "<<tic[1]<<" "<<tic[2]<<endl;

		//cout<<"dentro de matrixSum "<<tic[0]<<" "<<tic[1]<<" "<<c1<<" "<<c2<<endl;
		//STEP 13: Release OpenCL resources
		//Free OpenCL resources
/*				clReleaseKernel(kernel);
				clReleaseProgram(program);
				clReleaseCommandQueue(cmdQueue);
				clReleaseMemObject(bufferA);
				clReleaseMemObject(bufferB);
				clReleaseMemObject(bufferC);
				clReleaseContext(context);
*/
				//Free host resources
				//free(matA);
				//free(matB);
				//free(matC);
			//	free(h_C);
	//			free(platforms);
	//			free(devices);

				return ptoc;
	//return resultMatrix;
}
